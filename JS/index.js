// Логика кнопки "Читать далее"

const readHideBottom = document.querySelector('.read_bottom');
const hideHeroText = document.querySelector('.hero__content__text-n');
const linkImage = document.querySelector('.link-image');

readHideBottom.addEventListener('click', function(event) {
    event.preventDefault(); // Предотвращаем переход по ссылке
    
    if (hideHeroText.style.display === '' || hideHeroText.style.display === 'none') {
        hideHeroText.style.display = 'block';
        readHideBottom.textContent = 'Скрыть';
        linkImage.classList.add('rotated');
    } else {
        hideHeroText.style.display = 'none';
        readHideBottom.textContent = 'Читать далее';
        linkImage.classList.remove('rotated');
    }
});

//Логика меню бургер

const burger = document.querySelector('.burger');
const mainMenu = document.querySelector('.main-menu');
const mainContent = document.querySelector('.main-content');
const closeButton = document.querySelector('.menu__close-button');
const overlayContent = document.querySelector('.overlay')
let isMenuOpen = false;

function handleBurgerClick() {
    if (mainMenu.classList.contains('show-menu')) { // Логика закрытия меню-бургер
        overlayContent.classList.remove('overlay_active');
        closeButton.style.transform = 'scale(1)'; 
        isMenuOpen = false;
        console.log(111)
    } else {
        mainMenu.classList.add('show-menu'); // Логика открытия меню-бургер
        mainMenu.classList.remove('menu-closed');
        mainMenu.style.display = 'flex';
        overlayContent.classList.add('overlay_active');
        closeButton.style.transform = 'scale(1.2)'; 
        isMenuOpen = true;
        console.log(222)
    }
}

function handleResize() {
    if (window.innerWidth >= 320 && window.innerWidth <= 767) {
        burger.removeEventListener('click', handleBurgerClick);
        burger.addEventListener('click', handleBurgerClick);
        console.log(333)
    } else if (window.innerWidth >= 768 && window.innerWidth <= 1119) {
        burger.removeEventListener('click', handleBurgerClick);
        burger.addEventListener('click', handleBurgerClick);
        mainMenu.classList.remove('show-menu');
        mainMenu.style.display = 'none';
        isMenuOpen = false;
        console.log(444)
    } else {
        mainMenu.style.display = 'flex'; 
        mainMenu.classList.remove('menu-closed');
        mainMenu.classList.remove('move-left');
        console.log(555)
    }
    
    closeButton.addEventListener('click', function() {
        mainMenu.classList.remove('show-menu');
        mainMenu.classList.add('menu-closed');
        mainMenu.style.display = 'flex';
        isMenuOpen = false;
        overlayContent.classList.remove('overlay_active');
        console.log(666)
    });

    overlayContent.addEventListener('click', function() {
        if (window.innerWidth <= 1119) {
        mainMenu.style.display = 'none';
        overlayContent.classList.remove('overlay_active');
        mainMenu.classList.remove('show-menu');
        modalFormCall.classList.remove('show-modal-call');
            modalFormFeedback.classList.remove('show-modal-feedback');
        console.log(777)
        } else {
            overlayContent.classList.remove('overlay_active');
            mainMenu.classList.remove('show-menu');
            modalFormCall.classList.remove('show-modal-call');
            modalFormFeedback.classList.remove('show-modal-feedback');
            isMenuOpen = false;
        }
    });
}

handleResize();

window.addEventListener('resize', handleResize);

// Логика клика по услугам в бургер меню

const menuItems = document.querySelectorAll('.menu__service__nav__item');

menuItems.forEach(menuItem => {
    menuItem.addEventListener('click', function() {
        menuItems.forEach(item => item.classList.remove('menu__service__nav__item_active'));
        this.classList.add('menu__service__nav__item_active');
        
        const beforeElement = this.querySelector('::before');
        beforeElement.style.top = '-15%'; 
    });
});

//Инициализация Swiper

const swiperSettings = {
    spaceBetween: 16,
    slidesPerView: 'auto',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
};

let mySwiper; 

function initSwiper() {
    if (window.innerWidth < 768) {
        mySwiper = new Swiper('.swiper', swiperSettings);
    }
}

initSwiper();

// Добавляем обработчик изменения размера окна для переинициализации Swiper при достижении ширины 768px

window.addEventListener('resize', () => {
    if (window.innerWidth >= 768 && mySwiper) {
        mySwiper.forEach((swiper) => {
            swiper.destroy();
        });
        mySwiper = undefined;
    } else if (window.innerWidth < 768 && !mySwiper) {
        initSwiper();
    }
});

// Управление видимостью элементов слайдера для брендов
const flexItems1 = document.querySelectorAll('.swiper-slide.slide-1');
const showHideButton1 = document.querySelector('#show-hide-button-1');
const showHideImage1 = document.querySelector('#show-hide-button__image-1');

let isHidden1 = true; 
let numVisibleItems1 = 6; // Количество отображаемых элементов по умолчанию

const setNumVisibleItems1 = () => {  
    if (window.innerWidth >= 320 && window.innerWidth <= 767) {
        numVisibleItems1 = 11;
    } else if (window.innerWidth >= 768 && window.innerWidth <= 1119) {
        numVisibleItems1 = 6;
    } else {
        numVisibleItems1 = 8;
    }

    flexItems1.forEach((item, index) => {
        item.style.display = index < numVisibleItems1 ? 'flex' : 'none';
    });
};

// Установить количество отображаемых элементов при загрузке и при изменении размера окна
window.addEventListener('load', setNumVisibleItems1);
window.addEventListener('resize', setNumVisibleItems1);

//Кнопка "Показать все"/"Cкрыть все"
showHideButton1.addEventListener('click', function() {
    flexItems1.forEach((item, index) => {
        if (index >= numVisibleItems1) {
            item.style.display = isHidden1 ? 'flex' : 'none';
        }
    }); 

    isHidden1 = !isHidden1; 
    
    showHideButton1.textContent = isHidden1 ? 'Показать все' : 'Скрыть все';

    const rotationValue = isHidden1 ? '0deg' : '180deg';
    showHideImage1.style.transform = `rotate(${rotationValue})`;
    });

// Управление видимостью элементов слайдера для ремонта ноутбуков
const flexItems2 = document.querySelectorAll('.swiper-slide.slide-2');
const showHideButton2 = document.querySelector('#show-hide-button-2');
const showHideImage2 = document.querySelector('#show-hide-button__image-2');

let isHidden2 = true; 
let numVisibleItems2 = 3; 

const setNumVisibleItems2 = () => {  
    if (window.innerWidth >= 320 && window.innerWidth <= 767) {
        numVisibleItems2 = 9;
    } else if (window.innerWidth >= 768 && window.innerWidth <= 1119) {
        numVisibleItems2 = 3;
        
    } else {
        numVisibleItems2 = 4;
    }

    flexItems2.forEach((item, index) => {
        item.style.display = index < numVisibleItems2 ? 'flex' : 'none';
    });
};

// Установить количество отображаемых элементов при загрузке и при изменении размера окна
window.addEventListener('load', setNumVisibleItems2);
window.addEventListener('resize', setNumVisibleItems2);

//Кнопка "Показать все"/"Cкрыть все"
showHideButton2.addEventListener('click', function() {
    flexItems2.forEach((item, index) => {
        if (index >= numVisibleItems2) {
            item.style.display = isHidden2 ? 'flex' : 'none';
        }
    });

    isHidden2 = !isHidden2; 
    
    showHideButton2.textContent = isHidden2 ? 'Показать все' : 'Скрыть все';

    const rotationValue = isHidden2 ? '0deg' : '180deg';
    showHideImage2.style.transform = `rotate(${rotationValue})`;
});


/*Логика модального окна-звонок*/

const buttonCall = document.querySelectorAll('.open-modal__call');
const modalFormCall = document.querySelector('.modal-call');
const closeButtonFormCall = document.querySelector('.modal-call__button');
const closeOpenModalCall = document.querySelector('.close-open-modal-call')

let isFormOpen = false;

function openModalCall() {
    modalFormCall.classList.add('show-modal-call');
    isFormOpen = true;
    overlayContent.classList.add('overlay_active');    
}

function closeModalCall() {
    modalFormCall.classList.remove('show-modal-call');
    isFormOpen = false;
    
}

buttonCall.forEach(button => {
    button.addEventListener('click', () => {
        openModalCall();              
    });
});

closeButtonFormCall.addEventListener('click', () => {
    closeModalCall(); 
    overlayContent.classList.remove('overlay_active');
});

/*На разрешении до 1119 открытие модального окна и закрытие бургер меню*/

closeOpenModalCall.addEventListener('click', () => {
    if (window.innerWidth <= 1119) {
    mainMenu.classList.remove('show-menu');
    mainMenu.classList.add('menu-closed');
   
} else {
    overlayContent.classList.add('overlay_active');    
}
});

/*Логика модального окна-обратная связь*/

const buttonFeedback = document.querySelectorAll('.open-modal__feedback');
const modalFormFeedback = document.querySelector('.modal-feedback');
const closeButtonFormFeedback = document.querySelector('.modal-feedback__button');
const closeOpenModalFeedback = document.querySelector('.close-open-modal-feedback')

function openModalFeedback() {
    modalFormFeedback.classList.add('show-modal-feedback');
    isFormOpen = true;
    overlayContent.classList.add('overlay_active');    
}

function closeModalFeedback() {
    modalFormFeedback.classList.remove('show-modal-feedback');
    isFormOpen = false;    
}

buttonFeedback.forEach(button => {
    button.addEventListener('click', () => {
        openModalFeedback();              
    });
});

closeButtonFormFeedback.addEventListener('click', () => {
    closeModalFeedback(); 
    overlayContent.classList.remove('overlay_active');
});

/*На разрешении до 1119 открытие модального окна и закрытие бургер меню*/

closeOpenModalFeedback.addEventListener('click', () => {
    if (window.innerWidth <= 1119) {
    mainMenu.classList.remove('show-menu');
    mainMenu.classList.add('menu-closed');
   
} else {
    overlayContent.classList.add('overlay_active');    
}
});











